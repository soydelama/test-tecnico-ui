import { Component, OnDestroy, OnInit } from '@angular/core';

import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { NgForm } from '@angular/forms';

import { TemaService } from '../../shared/tema/tema.service';

@Component({
  selector: 'app-tema-edita',
  templateUrl: './tema-edita.component.html',
  styleUrls: ['./tema-edita.component.css']
})
export class TemaEditaComponent implements OnInit, OnDestroy {

  tema: any = {};
  sub: Subscription;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private temaService: TemaService) {
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      const id = params['id'];
      if (id) {
        this.temaService.get(id).subscribe((tema: any) => {
          tema = tema[0];
          if (tema) {
            this.tema = tema;
          } else {
            console.log(`Tema con id '${id}' no encontrado`);
            this.gotoList();
          }
        });
      }
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  gotoList() {
    this.router.navigate(['/listar-temas']);
  }

  saveForm(form: NgForm) {
    this.temaService.save(form).subscribe(result => {
        this.gotoList();
      }, error => console.error(error)
    );
  }

  ngAfterViewChecked() {
    var buttons = document.getElementsByTagName('button');
    for (var i = 0; i < buttons.length; i++) {
      var button = buttons[i];
      if (button.textContent=="Upload All" || button.textContent=="Remove All") {
        button.disabled=true;
      }
    }
  }

}
