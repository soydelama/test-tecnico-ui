import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemaEditaComponent } from './tema-edita.component';

describe('TemaEditaComponent', () => {
  let component: TemaEditaComponent;
  let fixture: ComponentFixture<TemaEditaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemaEditaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemaEditaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
