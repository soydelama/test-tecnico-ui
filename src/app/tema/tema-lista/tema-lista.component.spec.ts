import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemaListaComponent } from './tema-lista.component';

describe('TemaListaComponent', () => {
  let component: TemaListaComponent;
  let fixture: ComponentFixture<TemaListaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TemaListaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemaListaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
