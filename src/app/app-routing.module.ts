import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemaListaComponent } from './tema/tema-lista/tema-lista.component';
import { TemaEditaComponent } from './tema/tema-edita/tema-edita.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/listar-temas',
    pathMatch: 'full'
  },
  {
    path: 'listar-temas',
    component: TemaListaComponent
  },
  {
    path: 'tema-agregar',
    component: TemaEditaComponent
  },
  {
    path: 'tema-editar/:id',
    component: TemaEditaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
