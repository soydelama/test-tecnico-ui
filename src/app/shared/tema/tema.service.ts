import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TemaService {

  public API = '//localhost:8080';
  public TEMA_API = this.API + '/temas';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.TEMA_API);
  }

  save(tema: any): Observable<any> {
    let result: Observable<Object>;
    if (tema.id) {
      result = this.http.put(this.TEMA_API + '/' + tema.id, tema);
    } else {
      result = this.http.post(this.TEMA_API, tema);
    }
    return result;
  }

  get(id: string) {
    return this.http.get(this.TEMA_API + '/filtered?idTema=' + id);
  }

  getFiltered(descripcion: string, idEstadoSeleccionado: number): Observable<any> {
    if (descripcion=='undefined') {
      descripcion='';
    }
    return this.http.get(this.TEMA_API + '/filtered?descripcionTema=' + descripcion + '&idEstado=' + idEstadoSeleccionado);
  }

}
