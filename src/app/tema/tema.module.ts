import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemaListaComponent } from './tema-lista/tema-lista.component';
import { TemaEditaComponent } from './tema-edita/tema-edita.component';

@NgModule({
  declarations: [TemaListaComponent, TemaEditaComponent],
  imports: [
    CommonModule
  ],
  exports: [TemaListaComponent, TemaEditaComponent]
})
export class TemaModule { }
