import { Component, OnInit } from '@angular/core';
import { TemaService } from '../../shared/tema/tema.service';
import { EstadoService } from '../../shared/estado/estado.service';

@Component({
  selector: 'app-tema-lista',
  templateUrl: './tema-lista.component.html',
  styleUrls: ['./tema-lista.component.css']
})
export class TemaListaComponent implements OnInit {
  temas: Array<any>;
  estados: Array<any>;
  descripcionABuscar: string;
  estadoSeleccionado: number;

  constructor(private temaService: TemaService, private estadoService: EstadoService) {
    this.descripcionABuscar='';
    this.estadoSeleccionado=-1;
  }

  ngOnInit() {
    this.temaService.getAll().subscribe(data => {
      this.temas = data;
    });

    this.estadoService.getAll().subscribe(data => {
      this.estados = data;
      this.estados.push({"id":"-1", "descripcion":"Todos"});
    });
  }

  filtrar() {
    this.temaService.getFiltered(this.descripcionABuscar, this.estadoSeleccionado).subscribe(data => {
      this.temas = data;
    });
  }

}
