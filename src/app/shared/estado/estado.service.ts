import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {

  public API = '//localhost:8080';
  public ESTADO_API = this.API + '/estados';

  constructor(private http: HttpClient) { }

  getAll(): Observable<any> {
    return this.http.get(this.ESTADO_API);
  }

}
